const ACCOUNT = 'account'
export default [
    {
        path: `/${ACCOUNT}/settings`,
        name: "account-settings",
        component: () =>
          import(/* webpackChunkName: "login" */ "@/views/Account/Settings.vue")
    },
]