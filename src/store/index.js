import Vue from "vue";
import Vuex from "vuex";
import pathField from "@/helpers/pathField";

// Modules
// import account from "@/store/account/_index"
// import home from "@/store/home/_index"
// import radioroom from "@/store/radioroom/_index"
// import radiocast from "@/store/radiocast/_index"
Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    accessToken: null,
    authorized: false,
    innerWidth: null,
    screenSize: null,
    isOpen: false
  },
  mutations: {
    SET_DATA(state, { field, data }) {
      pathField(state, field, data);
    }
  },
  actions: {
    set_width(context) {
      const innerWidth = window.innerWidth;
      let currentScreenName = null;

      if (innerWidth < 576) {
        currentScreenName = 'xs';
      } else if (innerWidth < 767) {
        currentScreenName = 'sm';
      } else if (innerWidth < 992) {
        currentScreenName = 'md';
      } else if (innerWidth < 1200) {
        currentScreenName = 'lg';
      } else {
        currentScreenName = 'xl';
      }
      context.commit('SET_DATA', {
        field: 'innerWidth',
        data: innerWidth
      }, { root: true });

      context.commit('SET_DATA', {
        field: 'screenSize',
        data: currentScreenName
      }, { root: true })
    },
    set_is_open(context, isOpen) {
      context.commit('SET_DATA', {
        field: 'isOpen',
        data: isOpen
      }, { root: true })
    }
  },
  modules: {
    // account,
    // home,
    // radioroom,
    // radiocast
  }
});
