import {FETCH_RADIOROOM_PAGE} from "@/helpers/allRequests"
export default {
  FETCH_DATA(context, payload) {
    FETCH_RADIOROOM_PAGE(payload)
      .then(res => {console.log(res)})
      .catch(err => {console.log(err)})
  }
}