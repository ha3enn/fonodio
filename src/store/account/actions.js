import { GET_TEST } from "@/helpers/allRequests"

export default {
    /* POST_LOGIN(context, payload) {
        POST_LOGIN()
            // .then(res => {})
            // .catch(err => console.log(err))
    }, */
    GET_TEST(context) {
        GET_TEST()
            .then(res => {
                context.commit('SET_DATA', {
                    field: 'account.lovesTags',
                    data: res.data
                }, { root: true })
            })
            .catch(err => console.log(err))
    }
}