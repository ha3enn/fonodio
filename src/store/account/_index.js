import actions from './actions'
import getters from './getters'
export default {
    namespaced: true,
    state: {
        name: '',
        loginName: '',
        email: '',
        emailNotifications: '',
        lovesTags: [],
        hatesTags: []
    },
    actions,
    getters
}