export default {
    computed: {
        needHeaderCategories() {
            return this.$route.meta.needHeaderCategories;
        },
    },
    methods:{
        getCategoriesHeight(height){
            if(this.needHeaderCategories){
                this.$refs.firstSection.style.paddingTop = height + 'px';
            }
        },
    }
}