export const secCategories = [
    {
        id: 1,
        route: "",
        imgUrl: "/static/img/food.jpg",
        title: "foreign"
    },
    {
        id: 2,
        route: "",
        imgUrl: "/static/img/food.jpg",
        title: "tech"
    },
    {
        id: 3,
        route: "",
        imgUrl: "/static/img/food.jpg",
        title: "foreign"
    },
    {
        id: 4,
        route: "",
        imgUrl: "/static/img/food.jpg",
        title: "tech"
    },
    {
        id: 5,
        route: "",
        imgUrl: "/static/img/food.jpg",
        title: "foreign"
    },
    {
        id: 6,
        route: "",
        imgUrl: "/static/img/food.jpg",
        title: "tech"
    },
    {
        id: 7,
        route: "",
        imgUrl: "/static/img/food.jpg",
        title: "foreign"
    },
    {
        id: 8,
        route: "",
        imgUrl: "/static/img/food.jpg",
        title: "tech"
    }
]

export const episode = {
    id: 1,
    topics: [
        'sdsdsd',
        'sdsdsd',
        'sdsdsd'
    ],
    about: 'sdsdsd',
    featured: [
        {
            id: 1,
            route: "",
            imgUrl: "/static/img/radiostation.png",
            title: "foreign"
        },
        {
            id: 2,
            route: "",
            imgUrl: "/static/img/radiostation.png",
            title: "tech"
        },
        {
            id: 3,
            route: "",
            imgUrl: "/static/img/radiostation.png",
            title: "foreign"
        },
        {
            id: 4,
            route: "",
            imgUrl: "/static/img/radiostation.png",
            title: "tech"
        },
        {
            id: 5,
            route: "",
            imgUrl: "/static/img/radiostation.png",
            title: "foreign"
        },
        {
            id: 6,
            route: "",
            imgUrl: "/static/img/radiostation.png",
            title: "tech"
        },
    ]
};

export const catTags = [
    'Economics',
    'Lifestyle',
    'Sport',
    'Science',
    'Politics',
    'Foreign',
    'Entertainment',
    'Tech',
]

export const categories = [
    'Economics',
    'Lifestyle',
    'Sport',
    'Science',
    'Politics',
    'Foreign',
    'Entertainment',
    'Tech',
];

let date = new Date();
export const cardRooms = [
    {
        title: 'title1',
        imgURL: 'https://upload.wikimedia.org/wikipedia/commons/thumb/5/56/Donald_Trump_official_portrait.jpg/267px-Donald_Trump_official_portrait.jpg',
        date: date,
        time: '65',
        radiostation: {
            title: 'title',
            logoUrl: 'https://upload.wikimedia.org/wikipedia/commons/4/47/Logo_Pertama_BBS_TV_Surabaya.jpg'
        },
    },
    {
        title: 'title2',
        imgURL: 'https://upload.wikimedia.org/wikipedia/commons/thumb/5/56/Donald_Trump_official_portrait.jpg/267px-Donald_Trump_official_portrait.jpg',
        date: date,
        time: '4',
        radiostation: {
            title: 'title',
            logoUrl: 'https://upload.wikimedia.org/wikipedia/commons/4/47/Logo_Pertama_BBS_TV_Surabaya.jpg'
        }
    },
    {
        title: 'title3',
        imgURL: 'https://upload.wikimedia.org/wikipedia/commons/thumb/5/56/Donald_Trump_official_portrait.jpg/267px-Donald_Trump_official_portrait.jpg',
        date: date,
        time: '125',
        radiostation: {
            title: 'title',
            logoUrl: 'https://upload.wikimedia.org/wikipedia/commons/4/47/Logo_Pertama_BBS_TV_Surabaya.jpg'
        }
    },
    {
        title: 'title4',
        imgURL: 'https://upload.wikimedia.org/wikipedia/commons/thumb/5/56/Donald_Trump_official_portrait.jpg/267px-Donald_Trump_official_portrait.jpg',
        date: date,
        time: '22',
        radiostation: {
            title: 'title',
            logoUrl: 'https://upload.wikimedia.org/wikipedia/commons/4/47/Logo_Pertama_BBS_TV_Surabaya.jpg'
        }
    },
    {
        title: 'title5',
        imgURL: 'https://upload.wikimedia.org/wikipedia/commons/thumb/5/56/Donald_Trump_official_portrait.jpg/267px-Donald_Trump_official_portrait.jpg',
        date: date,
        time: '9',
        radiostation: {
            title: 'title',
            logoUrl: 'https://upload.wikimedia.org/wikipedia/commons/4/47/Logo_Pertama_BBS_TV_Surabaya.jpg'
        }
    },
    {
        title: 'title6',
        imgURL: 'https://upload.wikimedia.org/wikipedia/commons/thumb/5/56/Donald_Trump_official_portrait.jpg/267px-Donald_Trump_official_portrait.jpg',
        date: date,
        time: '47',
        radiostation: {
            title: 'title',
            logoUrl: 'https://upload.wikimedia.org/wikipedia/commons/4/47/Logo_Pertama_BBS_TV_Surabaya.jpg'
        }
    }
]

export const radiostations = [
    {
        id: 1,
        title: 'title',
        imgURL: '/static/img/radiostation.png',
    },
    {
        id: 2,
        title: 'title',
        imgURL: '/static/img/radiostation.png',
    },
    {
        id: 3,
        title: 'title',
        imgURL: '/static/img/radiostation.png',
    },
    {
        id: 3,
        title: 'title',
        imgURL: '/static/img/radiostation.png',
    },
    {
        id: 3,
        title: 'title',
        imgURL: '/static/img/radiostation.png',
    },
    {
        id: 3,
        title: 'title',
        imgURL: '/static/img/radiostation.png',
    },
    {
        id: 3,
        title: 'title',
        imgURL: '/static/img/radiostation.png',
    },
    {
        id: 3,
        title: 'title',
        imgURL: '/static/img/radiostation.png',
    },
    {
        id: 3,
        title: 'title',
        imgURL: '/static/img/radiostation.png',
    },
    {
        id: 3,
        title: 'title',
        imgURL: '/static/img/radiostation.png',
    },
    {
        id: 3,
        title: 'title',
        imgURL: '/static/img/radiostation.png',
    },
    {
        id: 3,
        title: 'title',
        imgURL: '/static/img/radiostation.png',
    },
    {
        id: 3,
        title: 'title',
        imgURL: '/static/img/radiostation.png',
    },
    {
        id: 3,
        title: 'title',
        imgURL: '/static/img/radiostation.png',
    },
    {
        id: 3,
        title: 'title',
        imgURL: '/static/img/radiostation.png',
    },
    {
        id: 3,
        title: 'title',
        imgURL: '/static/img/radiostation.png',
    },
    {
        id: 3,
        title: 'title',
        imgURL: '/static/img/radiostation.png',
    },
    {
        id: 3,
        title: 'title',
        imgURL: '/static/img/radiostation.png',
    },
    {
        id: 3,
        title: 'title',
        imgURL: '/static/img/radiostation.png',
    },
    {
        id: 3,
        title: 'title',
        imgURL: '/static/img/radiostation.png',
    },
    {
        id: 3,
        title: 'title',
        imgURL: '/static/img/radiostation.png',
    },
    {
        id: 3,
        title: 'title',
        imgURL: '/static/img/radiostation.png',
    },
    {
        id: 3,
        title: 'title',
        imgURL: '/static/img/radiostation.png',
    },
];

export const likesList = [
    {
        id: 1,
        name: 'test'
    },
    {
        id: 2,
        name: 'test'
    },
    {
        id: 3,
        name: 'test'
    },
    {
        id: 4,
        name: 'test'
    },
]