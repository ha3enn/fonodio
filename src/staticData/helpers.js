export const imgPath = '/static/img/';

export function getImagePath(imageName){
    return `${imgPath}${imageName}`
}
